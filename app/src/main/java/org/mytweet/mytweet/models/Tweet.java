package org.mytweet.mytweet.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.UUID;

public class Tweet
{
    public String id;
    public String message;
    public String date;

    private static final String JSON_ID = "id";
    private static final String JSON_MESSAGE = "message";
    private static final String JSON_DATE = "date";

    public Tweet()
    {
        id = UUID.randomUUID().toString();
        message = "";
        date = new Date().toString();
    }

    public Tweet(JSONObject json) throws JSONException {
        id = json.getString(JSON_ID);
        message = json.getString(JSON_MESSAGE);
        date = json.getString(JSON_DATE);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id.toString());
        json.put(JSON_MESSAGE, message);
        json.put(JSON_DATE, date);
        return json;
    }

    public String getTweetReport() {
        String report = "Tweet:  " + message + ".  Date: " + date;
        return report;
    }

    //Sets Tweet
    public void setText(String message)
    {
        this.message = message;
    }

    public String getDateString()
    {
        // return DateFormat.getDateTimeInstance().format(date);
        return date;
    }
}
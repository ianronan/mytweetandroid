package org.mytweet.mytweet.models;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static org.mytweet.mytweet.helpers.LogHelpers.info;

public class TweetList
{
    public  ArrayList<Tweet>  tweets;
    private TweetListSerializer serializer;

    public TweetList(TweetListSerializer serializer)
    {
        this.serializer = serializer;
        try
        {
            tweets = serializer.loadTweets();
        }
        catch (Exception e)
        {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }

    public boolean saveTweets()
    {
        try
        {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }

    public void addTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public Tweet getTweet(String id)
    {
        Log.i(this.getClass().getSimpleName(), "UUID parameter id: "+ id);

        for (Tweet twe : tweets)
        {
            if(id.equals(twe.id))
            {
                return twe;
            }
        }
        info(this, "failed to find tweet. returning first element array to avoid crash");
        return null;
    }

    public void deleteTweet(Tweet t)
    {
        tweets.remove(t);
    }

    public void deleteAllTweet()
    {
        tweets.removeAll(tweets);
    }

    public void updateTweets (List<Tweet> list)
    {
        tweets.clear();
        tweets.addAll(list);
        saveTweets();
    }
}
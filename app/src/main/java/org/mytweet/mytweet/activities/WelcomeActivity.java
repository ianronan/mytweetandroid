package org.mytweet.mytweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.mytweet.mytweet.R;
import org.mytweet.mytweet.app.MyTweetApp;
import org.mytweet.mytweet.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class WelcomeActivity extends Activity implements Callback<List<Tweeter>> {

    private MyTweetApp app;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);

        app = (MyTweetApp)getApplication();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        app.currentTweeter = null;
        Call<List<Tweeter>>call=(Call<List<Tweeter>>)app.tweetService.getAllTweeters();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit)
    {
        serviceAvailableMessage();
        app.tweeters = response.body();
        app.tweetServiceAvailable = true;
    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        serviceUnavailableMessage();
    }

    public void loginPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity (new Intent(this, Login.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    public void signupPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity (new Intent(this, Signup.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "Tweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }

    void serviceAvailableMessage()
    {
        Toast toast = Toast.makeText(this, "Tweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }
}
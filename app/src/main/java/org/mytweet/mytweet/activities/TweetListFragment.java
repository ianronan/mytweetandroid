package org.mytweet.mytweet.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.mytweet.mytweet.R;
import org.mytweet.mytweet.app.MyTweetApp;
import org.mytweet.mytweet.helpers.IntentHelper;
import org.mytweet.mytweet.models.Tweet;
import org.mytweet.mytweet.models.TweetList;
import org.mytweet.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class  TweetListFragment extends ListFragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener, Callback<List<Tweet>>
{
    private ListView listView;
    private TweetList tweetList;

    private TweetAdapter adapter;

    private Tweeter tweeter;
    private MyTweetApp app;

    private ArrayList<Tweet> tweets;

    private static final int SETTINGS_RESULT = 1;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name);

        app = (MyTweetApp) getActivity().getApplication();
        tweeter =app.currentTweeter;
        tweetList = app.tweetList;
        tweets = tweetList.tweets;

        adapter = new TweetAdapter(getActivity(), tweetList.tweets);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    @Override
    public void onListItemClick(ListView listView, View v, int position, long id)
    {
        Tweet twe = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetPagerActivity.class);
        i.putExtra(TweetFragment.EXTRA_TWEET_ID, twe.id);
        startActivityForResult(i, 0);

    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweetlist, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_new_tweet:
                Tweet tweet = new Tweet();
                tweetList.addTweet(tweet);

                Intent i = new Intent(getActivity(), TweetPagerActivity.class);
                i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;

            case R.id.action_settings:
                i = new Intent(getActivity(),SetPrefActivity.class);
                startActivityForResult(i, SETTINGS_RESULT);
                return true;


            default:                           return super.onOptionsItemSelected(item);
        }
    }

    private void deleteRemoteTweet(String id, String tweetId)
    {
        Call<Tweet> call = app.tweetService.deleteTweet(id, tweetId);
        call.enqueue(new Callback<Tweet>() {

            @Override
            public void onResponse(Response<Tweet> response, Retrofit retrofit)
            {
                Toast.makeText(getActivity(), "Tweet has been deleted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t)
            {
                Toast.makeText(getActivity(), "Failed to delete selected tweet", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void refreshTweetList()
    {
        Call<List<Tweet>> call = app.tweetService.getAllTweets();
        call.enqueue(new Callback<List<Tweet>>() {

            @Override
            public void onResponse(Response<List<Tweet>> response, Retrofit retrofit)
            {
                List<Tweet> list = response.body();
                app.tweetList.updateTweets(list);
                ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
                //adapter.notifyDataSetChanged();
                startActivity(new Intent(getActivity(), TweetListActivity.class));
                Toast.makeText(getActivity(), "Retrieved " + list.size() + " tweets", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable t)
            {
                Toast.makeText(getActivity(), "Failed to Refresh Tweet List", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteAllRemoteTweets()
    {
        Call<String> call = app.tweetService.deleteAllTweets();
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Response<String> response, Retrofit retrofit)
            {
                Toast.makeText(getActivity(), "All Tweets deleted: "+response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t)
            {
                Toast.makeText(getActivity(), "Failed to delete all tweets", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResponse(Response<List<Tweet>> response, Retrofit retrofit)
    {

    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast toast = Toast.makeText(getActivity(), "Error retrieving tweeta", Toast.LENGTH_LONG);
        toast.show();
    }

    /* ************ MultiChoiceModeListener methods (begin) *********** */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), TweetPagerActivity.class, "TWEET_ID", tweet.id);
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b)
    {

    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.tweet_list_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.menu_item_delete_tweet:
                for (int i = adapter.getCount() - 1; i >= 0; i--)
                {
                    if (listView.isItemChecked(i))
                    {
                        Tweet tw = adapter.getItem(i);
                        tweetList.deleteTweet(tw);
                        deleteRemoteTweet(app.currentTweeter.id, tw.id);
                    }
                }
                mode.finish();
                adapter.notifyDataSetChanged();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode)
    {

    }
    
  /* ************ MultiChoiceModeListener methods (end) *********** */
}

class TweetAdapter extends ArrayAdapter<Tweet>
{
    private Context context;
    public List<Tweet> tweets;

    public TweetAdapter(Context context, List<Tweet> tweets)
    {
        super(context, 0, tweets);
        this.context = context;
        this.tweets = tweets;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }
        Tweet twe = getItem(position);

        TextView message = (TextView) convertView.findViewById(R.id.tweet_list_item_tweetText);
        message.setText(twe.message);

        TextView dateTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_dateTextView);

        return convertView;
    }
}
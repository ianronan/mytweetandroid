package org.mytweet.mytweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.mytweet.mytweet.R;
import org.mytweet.mytweet.app.MyTweetApp;
import org.mytweet.mytweet.helpers.ContactHelper;
import org.mytweet.mytweet.models.Tweet;
import org.mytweet.mytweet.models.TweetList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static org.mytweet.mytweet.helpers.IntentHelper.navigateUp;


// Retrofit Callback interface
public class TweetFragment extends Fragment implements TextWatcher,
        OnClickListener,
        Callback<Tweet>
{
    public static   final String  EXTRA_TWEET_ID = "mytweet.TWEET_ID";

    private static  final int     REQUEST_CONTACT = 1;

    private TextView numberCounter;
    private EditText message;
    private ImageButton tweetButton;
    private Button contactButton;
    private Button emailButton;

    private Tweet tweet;
    private TweetList tweetList;
    private MyTweetApp app;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String tweId = (String) getArguments().getSerializable(EXTRA_TWEET_ID);

        app = (MyTweetApp) getActivity().getApplication();
        tweetList = app.tweetList;
        tweet = tweetList.getTweet(tweId);

    }

    // Implement the Callback methods
    @Override
    public void onResponse(Response<Tweet> response, Retrofit retrofit)
    {
        Tweet returnedTweet = response.body();
        // If returned tweet matches the tweet we created and transmitted to the server then success.
        if (tweet.equals(returnedTweet))
        {
            Toast.makeText(getActivity(), "Tweet created successfully", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(getActivity(), "Failed to create tweet", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast.makeText(getActivity(), "Failed to create tweet due to unknown network issue", Toast.LENGTH_LONG).show();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        updateControls(tweet);

        return v;
    }

    private void addListeners(View v)
    {
        numberCounter = (TextView) v.findViewById(R.id.numberCounter);
        message  = (EditText)   v.findViewById(R.id.message);
        contactButton = (Button)  v.findViewById(R.id.contactButton);
        emailButton = (Button)  v.findViewById(R.id.emailButton);
        tweetButton = (ImageButton)  v.findViewById(R.id.tweetButton);


        tweetButton.setOnClickListener(this);
        emailButton.setOnClickListener(this);
        contactButton.setOnClickListener(this);
        message.addTextChangedListener(this);

    }

    public void updateControls(Tweet tweet)
    {
        message.setText(tweet.message);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home: navigateUp(getActivity());
                return true;
            default:                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        tweetList.saveTweets();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        else
        if (requestCode == REQUEST_CONTACT)
        {
            String name = ContactHelper.getContact(getActivity(), data);
            contactButton.setText(name);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable)
    {
        tweet.setText(editable.toString());
    }



    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            //Tweet button pressed and a toast appears advising message sent
            case R.id.tweetButton:
                transmitTweet();
                Toast toast = Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT);
                toast.show();
                break;
        }
    }

    public void transmitTweet()
    {
        Call<Tweet> call = app.tweetService.createTweet(app.currentTweeter.id, tweet);
        call.enqueue(this);
    }

}
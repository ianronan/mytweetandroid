package org.mytweet.mytweet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import org.mytweet.mytweet.R;
import org.mytweet.mytweet.app.MyTweetApp;
import org.mytweet.mytweet.models.Tweet;
import org.mytweet.mytweet.models.TweetList;

import java.util.ArrayList;


public class TweetPagerActivity extends FragmentActivity implements ViewPager.OnPageChangeListener
{
    private ViewPager viewPager;

    private ArrayList<Tweet> tweets;
    private TweetList tweetList;

    private PagerAdapter pagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();
        setCurrentItem();
    }

    private void setTweetList()
    {
        MyTweetApp app = (MyTweetApp) getApplication();
        tweetList = app.tweetList;
        tweets = tweetList.tweets;
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {

    }

    @Override
    public void onPageSelected(int position)
    {

    }

    @Override
    public void onPageScrollStateChanged(int state)
    {

    }

    private void setCurrentItem() {
        String twe = (String) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < tweets.size(); i++) {
            if (tweets.get(i).id.toString().equals(twe.toString())) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }
}

class PagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Tweet> tweets;

    public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
    {
        super(fm);
        this.tweets = tweets;
    }

    @Override
    public int getCount()
    {
        return tweets.size();
    }

    @Override
    public Fragment getItem(int pos)
    {
        Tweet tweet = tweets.get(pos);
        Bundle args = new Bundle();
        args.putSerializable(TweetFragment.EXTRA_TWEET_ID, tweet.id);
        TweetFragment fragment = new TweetFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
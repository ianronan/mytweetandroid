package org.mytweet.mytweet.activities;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.TextView;

import org.mytweet.mytweet.R;

public class SettingsActivity extends Activity
{
    TextView settings_username;
    TextView settings_password;
    TextView settings_refreshInterval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        settings_username.setText(sharedPreferences.getString("settings_username", "NOUSERNAME"));
        settings_password.setText(sharedPreferences.getString("settings_password", "NOPASSWORD"));
        settings_refreshInterval.setText(sharedPreferences.getString("settings_refreshInterval", "NOREFRESHINTERVAL"));
    }
}
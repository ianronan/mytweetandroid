package org.mytweet.mytweet.activities;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import org.mytweet.mytweet.R;

public class SettingsPreferences extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }
}
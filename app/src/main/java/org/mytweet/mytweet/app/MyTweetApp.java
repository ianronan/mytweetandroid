package org.mytweet.mytweet.app;


import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.mytweet.mytweet.main.MyTweetServiceProxy;
import org.mytweet.mytweet.models.Tweet;
import org.mytweet.mytweet.models.TweetList;
import org.mytweet.mytweet.models.TweetListSerializer;
import org.mytweet.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

import static org.mytweet.mytweet.helpers.LogHelpers.info;


public class MyTweetApp extends Application
{
    public String          service_url  = "http://10.0.2.2:9000";    //Android Emulator
    //public String          service_url  = "http://10.0.3.2:9000";    //Genymotion
    //public String          service_url  = "https://donation-service-2015.herokuapp.com/";

    public MyTweetServiceProxy tweetService;

    public boolean tweetServiceAvailable = false;

    public TweetList tweetList;

    public Tweeter currentTweeter;

    public List<Tweeter> tweeters = new ArrayList<Tweeter>();
    public List<Tweet> tweets = new ArrayList<Tweet>();


    private static final String FILENAME = "tweetList.json";

    @Override
    public void onCreate()
    {
        super.onCreate();
        TweetListSerializer serializer = new TweetListSerializer(this, FILENAME);
        tweetList = new TweetList(serializer);

        info(this, "MyTweet app launched");

        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        tweetService = retrofit.create(MyTweetServiceProxy.class);

    }

    public void newTweeter(Tweeter tweeter)
    {
        tweeters.add(tweeter);
    }

    public boolean validTweeter(String email, String password)
    {
        for (Tweeter tweeter : tweeters)
        {
            if(tweeter.email.equals(email) && tweeter.password.equals(password))
            {
                currentTweeter = tweeter;
                return true;
            }
        }
        return false;
    }
}
package org.wit.ictskills.models;

import wit.org.ictskills.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface MyTweetServiceProxy
{

    // Create a tweeter
    @POST("/api/tweeters")
    Call<Tweeter> createTweeter(@Body Tweeter tweeter);

    // Get all tweets
    @GET("/api/tweets")
    Call<List<Tweet>> getAllTweets();

    // Create a tweet
    @POST("/api/tweeters/{id}/tweets")
    Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

}
